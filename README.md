# Autómatas y Lenguajes Formales 2021-2


## Máquina Virtual

Usaremos esta máquina virtual que tiene instalado un entorno Python Anaconda con Jupyter Notebooks, el cual se hace visible al ingresar en el navegador la URL: http://localhost:9999 una vez que la máquina arranca.


La máquina virtual puede descargarse aquí: https://drive.google.com/file/d/1zEGShPTQqZ_skcVoxF-lULoqEZJcQTc6/view?usp=sharing


<!--## Google Colaboratory

Durante el periodo académico utilizaremos la plataforma Google Colaboratory para el desarrollo de talleres y parciales. El único requisito es que usted cuente con un correo @gmail.com, el cual le permitirá utilizar Google Drive, Google Docs y otras herramientas.-->

## Calificación

10% Taller 1<br/>
10% Taller 2 (partes (a) y (b))<br/>
10% Taller 3<br/>
20% Parcial 1<br/>
20% Parcial 2<br/>
20% Parcial 3<br/>
10% Proyecto final<br/>



## Talleres

Los talleres pretender ser una herramienta practica para afianzar los conocimientos desarrollados durante las clases. En general se presentan como un conjunto de ejercicios que serán desarrollados **individualmente** por los estudiantes. Cada uno de los talleres se desarrollará en casa, dentro de las fechas establecidas en el cronograma.


## Parciales

Son evaluaciones **individuales** basadas en notebooks sobre los temas tratados en las clases. Los estudiantes deben solucionarlo en el salón de clase, en un tiempo determinado. Durante los parciales, únicamente es posible utilizar los apuntes y notebooks del curso.


## Proyecto final
Se aceptarán grupos de máximo 3 estudiantes. Realizar el proyecto de forma individual es también aceptado.
- 20% Implementación
- 20% Definición formal y diagramas de transiciones correspondientes
- 20% Presentación siguiendo la plantilla oficial del evento SystemsFest, en donde se muestre: título del proyecto, abstract (resumen), introducción, propuesta y/o resultados. Debe enviarse en formato PDF.<br>
- 20% Información complementaria:<br/>
Imagen PNG o JPG que contenga la siguiente información: título del proyecto e información de los estudiantes. Debe tener una foto de fondo que esté relacionada directamente con el tema de su proyecto.<br/><br/>
Video de **MÁXIMO 4 minutos** en donde se presenta el proyecto. Se debe enviar el archivo del video (formato *.mp4 en lo posible).<br/>
- 20% Sustentación y puntualidad en la fecha y hora asignada

Nota: todos estos ítems deberán ser alojados en un repositorio (github, gitlab, etc) de alguno de los estudiantes del grupo.


## Calendario y plazos

                        SESSION 1                  SESSION 2              SESSION SATURDAY

     W01 Nov01-Nov05    Intro (a)                  Intro (b)
     W02 Nov08-Nov12    Python (a)                 Python (b)
     W03 Nov15-Nov19    Introducción               FSM Designer + JFLAP
     W04 Nov22-Nov26    Alfabetos y Lenguajes      AFD
     W05 Nov29-Dic03    REPASO                     REPASO
     W06 Dic06-Dic10    Práctica AFD               AFN
     W07 Dic13-Dic17    Práctica AFN               AFN-e
     ...................    VACACIONES    ..................... 
     W08 Ago23-Ago27    Práctica AFN-e             Expresiones Regulares
     W09 Ago30-Sep03    Propiedades E.R.           Conversión E.R. (a)
     W10 Sep06-Sep10    Conversión E.R. (b)        Minimización (a)
     W11 Sep13-Sep17    Minimización (b)           Gramáticas GLC (a)
     W12 Sep20-Sep24    Gramáticas GLC (b)         PRE SUSTENTACIÓN
     W13 Sep27-Oct01    Arboles Derivación         Práctica NLTK
     W14 Oct04-Oct08    Autómatas de Pila (a)      Autómatas de Pila (b)
     W15 Oct11-Oct15    Práctica PDA               Ejercicios
     W16 Oct18-Oct22    Intro MT                   MT (I)
     W17 Oct25-Oct29    MT (II)                    MT (III)


     Ene 14 -           -> Registro primera nota
     Ene 14 -           -> Último día cancelación materias
     Mar 11             -> Finalización clase
     Mar 22 - Oct 23    -> Habilitaciones
     Mar 19 -           -> Registro calificaciones finales

    

<!--MT Multibanda Multicinta   TBA-->

<!-- Ver el Calendario academico 2019:
https://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2019/acuerdoAcad064_2019.pdf-->

<!--[Calendario academico](https://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2019/acuerdoAcad314-2019.pdf)-->

<!--[Calendario academico 2020-1](https://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2020/acuerdoAcad104_2020.pdf)-->

<!--[Calendario academico 2020-2](https://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2020/acuerdoAcad294_2020.pdf)-->

<!--[Calendario academico 2021-1](http://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2020/acuerdoAcad434_2020.pdf)-->

<!--[Calendario academico 211 del 2021](http://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2021/acuerdoAcad211_2021.pdf)-->

[Calendario academico 277 del 2021](https://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2021/acuerdoAcad277_2021.pdf)


